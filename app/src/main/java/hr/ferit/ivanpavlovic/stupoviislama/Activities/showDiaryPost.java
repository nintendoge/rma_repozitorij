package hr.ferit.ivanpavlovic.stupoviislama.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.widget.TextView;

import hr.ferit.ivanpavlovic.stupoviislama.R;

public class showDiaryPost extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_show_diary_post);

        TextView TVusername = (TextView) findViewById(R.id.tvdbUsername);
        TextView TVdate = (TextView) findViewById(R.id.tvdbDiaryDate);
        TextView TVAyat = (TextView) findViewById(R.id.tvdbAyetDiary);
        TextView TVHadith = (TextView) findViewById(R.id.tvdbHadithDiary);
        TextView TVPost = (TextView) findViewById(R.id.tvdbPostFast);
        TextView TVNamaz = (TextView) findViewById(R.id.tvdbNamazCounter);



        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        String username= bundle.getString("username");
        String fast= bundle.getString("Fast");
        String Date= bundle.getString("Date");
        String Ayat= bundle.getString("Ayat");
        String Hadith= bundle.getString("Hadith");
        String Namaz= bundle.getString("Namaz");

        float f= Float.parseFloat(Namaz);
        f=f*100;


        if(fast.contains("false"))
        {
            fast="NE";
        }
        else if(fast=="true")
        {
            fast="DA";
        }

        TVusername.setText(username);
        TVdate.setText(Date);
        TVAyat.setText(Ayat);
        TVHadith.setText(Hadith);
        TVPost.setText(fast);
        TVNamaz.setText(String.format("%.0f%%",f));

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        /*koliko posto sirine i visine ekrana zelimo da bude popup window*/
        getWindow().setLayout((int) (width*.8), (int) (height*.6));
    }


}
