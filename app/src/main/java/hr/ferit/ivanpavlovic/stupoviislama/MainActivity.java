package hr.ferit.ivanpavlovic.stupoviislama;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    /*varijabla za provjeru imamo li dobru verziju APIja*/
    private static final int ERROR_DIALOG_REQUEST =9001;

    @Override
    public void onBackPressed() {
        onPostResume();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        String str = "For Diary usage please log in in the upper right corner";
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();


        final TextView tvSelam = (TextView) findViewById(R.id.tvSelam);
        Button bAyat = (Button) findViewById(R.id.bAyat);
        Button bMapa = (Button) findViewById(R.id.bMapa);


        Intent intent = getIntent();
        String name =  intent.getStringExtra("name");
        String surname =  intent.getStringExtra("surname");
        String username =  intent.getStringExtra("username");



        String message =  "Selam aleykum";
        tvSelam.setText(message);



        bAyat.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), ayat.class);
                startActivity(explicitIntent);
            }
        });

        bMapa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), qiblah.class);
                startActivity(explicitIntent);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MainActivity.this);

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            /*google nam daje soluciju za postojani problem u obliku dijaloga*/
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(MainActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }else{
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        /*ako postoji problem vraća false*/
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.Register:
                Intent i = new Intent(this, Register.class);
                startActivity(i);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case R.id.Login:
                Intent i2 = new Intent(this, Login.class);
                startActivity(i2);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            default:
                Toast.makeText(getApplicationContext(), "Unknown error occurred.", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
