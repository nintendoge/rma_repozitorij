package hr.ferit.ivanpavlovic.stupoviislama.klase;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;

import hr.ferit.ivanpavlovic.stupoviislama.R;

/**
 * Created by Iohannes on 12.8.2018..
 */

public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder> {
    /**/
    private ArrayList<ExampleItem> mExampleList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        /*postition koji item je kliknut, on item click nije koristen za sada*/

        void onToggleClick(int position);
        void disableAlarm(int position);
        void displayInfo(int position);

    }

    /*setta na listener koji mu je providan iz druge funkcije*/
    public void setOnItemClickListener(OnItemClickListener listener) { mListener=listener;}


    public static class ExampleViewHolder extends  RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTextView1;
        public TextView mTextView2;
        public ToggleButton mToggle;

        public ExampleViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            mImageView = (ImageView) itemView.findViewById(R.id.card_imageView);
            mTextView1 = (TextView) itemView.findViewById(R.id.card_textView);
            mTextView2 = (TextView) itemView.findViewById(R.id.card_textView2);
            mToggle = (ToggleButton) itemView.findViewById(R.id.toggleButton);

            mTextView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    listener.displayInfo(position);

                }
            });


            mToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int position = getAdapterPosition();
                    if (isChecked && position!=RecyclerView.NO_POSITION) {

                        listener.onToggleClick(position);


                    } else {
                        listener.disableAlarm(position);
                    }
                }
            });


        }
    }


    /*kada god se kreira adapter potrebno je prosljediti array listu*/
    public ExampleAdapter(ArrayList<ExampleItem> exampleList){
        mExampleList= exampleList;
    }


    /*implenetira metode*/
    @Override
    public ExampleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.example_item,parent,false);
        ExampleViewHolder evh = new ExampleViewHolder(v, mListener);
        return  evh;
    }

    @Override
    public void onBindViewHolder(ExampleViewHolder holder, int position) {
        /*prenošenje vrijednosti u elemente, position oznacava koji je element, 0. je prvi*/
        ExampleItem currentItem= mExampleList.get(position);

        holder.mImageView.setImageResource(currentItem.getImageResource());
        holder.mTextView1.setText(currentItem.getText1());
        holder.mTextView2.setText(currentItem.getText2());
    }

    @Override
    public int getItemCount() {
        /*definira koliko ce biti elemenata u listi*/
        return mExampleList.size();
    }
}