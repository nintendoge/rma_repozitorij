package hr.ferit.ivanpavlovic.stupoviislama.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import hr.ferit.ivanpavlovic.stupoviislama.R;
import hr.ferit.ivanpavlovic.stupoviislama.klase.DatabaseHelper;
import hr.ferit.ivanpavlovic.stupoviislama.klase.diary_popup;

public class Diary extends AppCompatActivity {
    DatabaseHelper mDatabaseHelper;
    ListView LVDiary;
    String UsernamefromPrefs;



    @Override
    protected void onPostResume() {
        super.onPostResume();
        populateListview();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);

        Button bAdd = (Button) findViewById(R.id.bDnevnikAdd);
         LVDiary = (ListView) findViewById(R.id.LVDiaryListView);
         mDatabaseHelper = new DatabaseHelper(this);

        SharedPreferences prefs = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);
        UsernamefromPrefs = prefs.getString("name", "No name defined");

        populateListview();

        Toast.makeText(getApplicationContext(),"Hold the date entry to delete it", Toast.LENGTH_LONG).show();

        bAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), diary_popup.class);
                startActivity(explicitIntent);
            }
        });
    }

    private void  populateListview(){
        //get the data and append it to the list

        Cursor data = mDatabaseHelper.getData();
        ArrayList<String> listData = new ArrayList<>();
        while (data.moveToNext()){
            //postavlja vrijednost datuma za onog korisnika koji je trenutno logiran
            if(Objects.equals(data.getString(1),UsernamefromPrefs))
            {
                listData.add(data.getString(2));
            }
        }
        //create the list adapter and set the adapter
        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        LVDiary.setAdapter(adapter);

        LVDiary.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String date = adapterView.getItemAtPosition(i).toString();

                String UsernamePost = mDatabaseHelper.getUsername(date);
                String FastPost= mDatabaseHelper.getFast(date);
                String HadithPost= mDatabaseHelper.getHadith(date);
                String AyatPost= mDatabaseHelper.getAyat(date);
                String NamazPost=mDatabaseHelper.getNamaz(date);



                Intent explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), showDiaryPost.class);
                explicitIntent.putExtra("username",UsernamePost );
                explicitIntent.putExtra("Fast",FastPost );
                explicitIntent.putExtra("Ayat",AyatPost );
                explicitIntent.putExtra("Hadith",HadithPost);
                explicitIntent.putExtra("Date",date );
                explicitIntent.putExtra("Namaz",NamazPost);
                startActivity(explicitIntent);



            }
        });


        LVDiary.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final String date = adapterView.getItemAtPosition(i).toString();

                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(Diary.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(Diary.this);
                }
                builder.setTitle("Delete entry")
                        .setMessage("Are you sure you want to delete this entry?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mDatabaseHelper.deleteRow(date);
                                onPostResume();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                onPostResume();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();



                return true;
            }
        });
    }





}


/*
*
*
*
*
*
* */