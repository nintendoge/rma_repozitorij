package hr.ferit.ivanpavlovic.stupoviislama.klase;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import hr.ferit.ivanpavlovic.stupoviislama.Activities.Diary;
import hr.ferit.ivanpavlovic.stupoviislama.Activities.namaz;
import hr.ferit.ivanpavlovic.stupoviislama.Login;
import hr.ferit.ivanpavlovic.stupoviislama.R;
import hr.ferit.ivanpavlovic.stupoviislama.Register;
import hr.ferit.ivanpavlovic.stupoviislama.RegisterRequest;
import hr.ferit.ivanpavlovic.stupoviislama.ayat;

public class diary_popup extends Activity {


/*    RequestQueue requestQueue;
    public static final String INSERT_REQUEST_URL = "http://unadvised-welds.000webhostapp.com/diary_input.php";*/
    DatabaseHelper mDatabaseHelper;
    public String username;
    public String ayatPref;
    public String hadithPref;
    public Date post_date;
    public boolean Fast;
    public float counter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diary_popup);
        mDatabaseHelper = new DatabaseHelper(this);
        UIsetup();
        Toast.makeText(getApplicationContext(), "This view is scrollable", Toast.LENGTH_LONG).show();
    }

    public void UIsetup()
    {
            /*setting username and ayat hadith values*/
            SharedPreferences prefs = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);
            username = prefs.getString("name", "No name defined");
            ayatPref = prefs.getString("ayat", "No name defined");
            hadithPref = prefs.getString("hadith", "No name defined");
            counter = prefs.getFloat("counter",0.0f);


            /*setting date*/
            final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            post_date = new Date();

            /*setting namaz percentage*/
            counter=counter*100;

            TextView tvUsername = (TextView) findViewById(R.id.tvDiaryUsername);
            TextView tvDate = (TextView) findViewById(R.id.tvDiaryDate);
            TextView tvAyetDiary = (TextView) findViewById(R.id.tvAyetDiary);
            TextView tvHadithDiary = (TextView) findViewById(R.id.tvHadithDiary);
            TextView tvNamazCounter = (TextView) findViewById(R.id.tvNamazCounter);
            ToggleButton toggleDiary = (ToggleButton) findViewById(R.id.toggleButton);
            Button bPost = (Button) findViewById(R.id.bPost);

            tvUsername.setText(username);
            tvAyetDiary.setText(ayatPref);
            tvHadithDiary.setText(hadithPref);
            tvDate.setText(dateFormat.format(post_date));
            tvNamazCounter.setText(String.format("%.0f%%",counter));



            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            int width = dm.widthPixels;
            int height = dm.heightPixels;
            /*koliko posto sirine i visine ekrana zelimo da bude popup window*/
            getWindow().setLayout((int) (width * .85), (int) (height * .70));


            toggleDiary.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        Fast = true;
                    } else {
                        Fast = false;
                    }
                }
            });




            bPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*prosiriti za ostale parametre*/

                    AddData(username, dateFormat.format(post_date), Boolean.toString(Fast), hadithPref, ayatPref, String.valueOf(counter));




                }
            });





    }





    public void AddData(String username,String post_date,String fast,String hadith,String ayet, String namaz) {
        boolean insertData = mDatabaseHelper.addData(username,post_date,fast, hadith, ayet, namaz);

        if (insertData) {
            toastMessage("Data Successfully Inserted!");
        } else {
            toastMessage("Something went wrong");
        }
    }

    /**
     * customizable toast
     * @param message
     */
    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }


}






