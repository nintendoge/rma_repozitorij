package hr.ferit.ivanpavlovic.stupoviislama.klase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import hr.ferit.ivanpavlovic.stupoviislama.R;

public class Alarm extends BroadcastReceiver{

    private static final int uniqueID=1684;
    private static final String CHANNEL_ID= "channel_param";

    @Override
    public void onReceive(Context context, Intent intent) {

        String name = intent.getStringExtra("name") ;
        Toast.makeText(context,"ALARM for namaz: "+name, Toast.LENGTH_LONG).show();
        Vibrator v= (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
        v.vibrate(3000);


        /*NOTIFICATION*/

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,CHANNEL_ID);
        builder.setSmallIcon(R.drawable.namaz_icon);
        builder.setContentTitle(name);
        builder.setContentText("Alarm was set off for "+name);

        builder.setWhen(System.currentTimeMillis());
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(uniqueID,builder.build());


    }


}
