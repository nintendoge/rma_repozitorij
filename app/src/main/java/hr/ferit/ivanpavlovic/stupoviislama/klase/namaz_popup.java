package hr.ferit.ivanpavlovic.stupoviislama.klase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.widget.TextView;
import android.widget.Toast;

import hr.ferit.ivanpavlovic.stupoviislama.R;

public class namaz_popup extends Activity{



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extra =getIntent().getExtras();
        String name=extra.getString("name");

        setContentView(R.layout.namaz_popup);

        ViewPager viewPager;


        TextView tvName = (TextView) findViewById(R.id.tv_popup);
        TextView tvDescription = (TextView) findViewById(R.id.tv_popup2);




        tvName.setText(name);

        switch (name)
        {
            case "Fajar":
                tvDescription.setText("Worship from dawn untill sunrise, 2 obligatory prayer cycles");
                break;

            case "Zohar":
                tvDescription.setText("After midday until Asar,4 obligatory prayer cycles");
                break;
            case "Asar":
                tvDescription.setText("Afternoon, 4 obligatory prayer cycles");
                break;
            case "Moghrib":
                tvDescription.setText("Worship from sunset until twilight, 3 obligatory prayer cycles");
                break;
            case "Isha":
                tvDescription.setText("Worship from twilight untill dawn, 4 obligatory prayer cycles");
                break;
            case "Juma":
                tvDescription.setText("Unobligatory worship in which one does 2 prayer cycles");
                break;

            default: Toast.makeText(getApplicationContext(), "invalid input", Toast.LENGTH_LONG).show();
                break;

        }



        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        /*koliko posto sirine i visine ekrana zelimo da bude popup window*/
        getWindow().setLayout((int) (width*.8), (int) (height*.25));
    }
}
