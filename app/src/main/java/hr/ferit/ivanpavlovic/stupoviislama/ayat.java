package hr.ferit.ivanpavlovic.stupoviislama;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.ferit.ivanpavlovic.stupoviislama.R;

public class ayat extends AppCompatActivity {


    TextView tvHadith;
    TextView tvAyat;
    ArrayList<String> titles;
    ArrayList<String> links;
    ArrayList<String> hadithQuote;
    ArrayList<String> hadithQuoteLink;
    public String str;
    public String str2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ayat_activity);

        tvHadith= (TextView) findViewById(R.id.tvHadith);
        tvAyat= (TextView) findViewById(R.id.tvAyat);

        titles = new ArrayList<String>();
        links = new ArrayList<String>();

        hadithQuote = new ArrayList<String>();
        hadithQuoteLink = new ArrayList<String>();


        Toast.makeText(this, "Pritisnite na tekst ayata/haditha za online prikaz", Toast.LENGTH_SHORT).show();

        tvAyat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(links.get(0));
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(intent);
            }
        });

        tvHadith.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(hadithQuoteLink.get(0));
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(intent);
            }
        });

        new ProcessInBackground().execute();
    }

    public InputStream getInputStream(URL url)
    {
        try
        {       /*open connection otvara određeni http*/
            return url.openConnection().getInputStream();
        }
        catch (IOException e)
        {
            return null;
        }
    }

    public String getStr() {

        return str;
    }

    public String getStr2() {
        return str2;
    }

    public class ProcessInBackground extends AsyncTask<Integer,Void,Exception>
    {
        ProgressDialog progressDialog = new ProgressDialog(ayat.this);

        Exception exception = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Selam aleykum");
            progressDialog.show();
        }

        @Override
        protected Exception doInBackground(Integer... params) {

            try
            {
                URL url = new URL ("http://www.haqvoice.com/feedsWriter/feeds/ayat.xml");
                URL url2 = new URL ("http://www.haqvoice.com/feedsWriter/feeds/hadith.xml");

                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                XmlPullParserFactory factory2 = XmlPullParserFactory.newInstance();

                factory.setNamespaceAware(false);
                factory2.setNamespaceAware(false);

                XmlPullParser xpp = factory.newPullParser();
                XmlPullParser xpp2 = factory2.newPullParser();

                xpp.setInput(getInputStream(url),"UTF_8");
                xpp2.setInput(getInputStream(url2),"UTF_8");

                /*varijabla koja govori kada smo u item tagu*/
                boolean insideItem = false;
                boolean insideItem2 = false;

                /*vraca pocetak ili kraj taga ili itema  itd, govori gdje smo*/
                int eventType = xpp.getEventType();
                int eventType2 = xpp2.getEventType();

                while(eventType != XmlPullParser.END_DOCUMENT)
                {
                    if (eventType == XmlPullParser.START_TAG)
                    {
                        if(xpp.getName().equalsIgnoreCase("item"))
                        {
                            insideItem=true;

                        }
                        /*ovaj dio loopa dohvaca ono sto zelimo*/
                        else if (xpp.getName().equalsIgnoreCase("description"))
                        {
                            if (insideItem)
                            {
                                /*ako smo u itemu u tagu petlja*/
                                titles.add(xpp.nextText());

                            }
                        }
                        else if (xpp.getName().equalsIgnoreCase("link"))
                        {
                            if (insideItem)
                            {
                                /*ako smo u itemu u tagu petlja*/
                                links.add(xpp.nextText());

                            }
                        }
                    }
                    else if (eventType == XmlPullParser.START_TAG && xpp.getName().equalsIgnoreCase("item"))
                    {
                        insideItem = false;
                    }
                    /*inkrement za petlju*/
                    eventType = xpp.next();
                }

                while(eventType2 != XmlPullParser.END_DOCUMENT)
                {
                    if (eventType2 == XmlPullParser.START_TAG)
                    {
                        if(xpp2.getName().equalsIgnoreCase("item"))
                        {
                            insideItem2=true;

                        }
                        /*ovaj dio loopa dohvaca ono sto zelimo*/
                        else if (xpp2.getName().equalsIgnoreCase("description"))
                        {
                            if (insideItem2)
                            {
                                /*ako smo u itemu u tagu petlja*/
                                hadithQuote.add(xpp2.nextText());

                            }
                        }
                        else if (xpp2.getName().equalsIgnoreCase("link"))
                        {
                            if (insideItem2)
                            {
                                /*ako smo u itemu u tagu petlja*/
                                hadithQuoteLink.add(xpp2.nextText());

                            }
                        }
                    }
                    else if (eventType2 == XmlPullParser.START_TAG && xpp2.getName().equalsIgnoreCase("item"))
                    {
                        insideItem2 = false;
                    }
                    /*inkrement za petlju*/
                    eventType2 = xpp2.next();
                }

            }
            catch (MalformedURLException e)
            {
                exception = e;
            }
            catch (XmlPullParserException e)
            {
                exception = e;
            }
            catch (IOException e)
            {
                exception = e;
            }


            return exception;
        }

        @Override
        protected void onPostExecute(Exception s) {
            super.onPostExecute(s);


            String[] stockArr = new String[hadithQuote.size()];
            stockArr = hadithQuote.toArray(stockArr);
            str = Arrays.toString(stockArr);
            str=str.replaceAll("\\[" ," ");
            str=str.replaceAll("\\]" ," ");
            tvHadith.setText(str);

            String[] stockArr2 = new String[titles.size()];
            stockArr2 = titles.toArray(stockArr2);
            str2 = Arrays.toString(stockArr2);
            str2=str2.replaceAll("\\[" ,"");
            str2=str2.replaceAll("\\]" ," ");
            tvAyat.setText(str2);


            SharedPreferences.Editor settings = getSharedPreferences("MyPrefsFile", MODE_PRIVATE).edit();
            settings.putString("ayat", str);
            settings.putString("hadith", str2);
            settings.apply();

            progressDialog.dismiss();
        }
    }


}
