package hr.ferit.ivanpavlovic.stupoviislama.klase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    private static final String TABLE_NAME = "diary2";
    private static final String COL1 = "ID";
    private static final String COL2 = "username";
    private static final String COL3 = "post_date";
    private static final String COL4 = "fast";
    private static final String COL5 = "hadith";
    private static final String COL6 = "ayat";
    private static final String COL7 = "namaz";


    public DatabaseHelper(Context context){

        super(context, TABLE_NAME,null,1);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL2 +" TEXT, " + COL3 + " TEXT, " + COL4 + " TEXT, " + COL5 + " TEXT, " + COL6 + " TEXT, " + COL7 + " TEXT)";
        sqLiteDatabase.execSQL(createTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + TABLE_NAME + "'");
        onCreate(sqLiteDatabase);
    }

    /*prosiriti za vise varijabli*/
    public boolean addData(String username,String post_date,String fast,String hadith,String ayet,String namaz) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2, username);
        contentValues.put(COL3, post_date);
        contentValues.put(COL4, fast);
        contentValues.put(COL5, hadith);
        contentValues.put(COL6, ayet);
        contentValues.put(COL7, namaz);


        long result = db.insert(TABLE_NAME, null, contentValues);

        //if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Returns all the data from database
     * @return
     */
    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }



    public String getUsername(String date){
        String Username = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COL2 + " FROM " + TABLE_NAME +
                " WHERE " + COL3 + " = '" + date + "'";
        Cursor data = db.rawQuery(query, null);
        while(data.moveToNext()){
            Username = data.getString(0);

        }
        return Username;
    }

    public String getFast(String date){
        String Fast = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COL4 + " FROM " + TABLE_NAME +
                " WHERE " + COL3 + " = '" + date + "'";
        Cursor data = db.rawQuery(query, null);
        while(data.moveToNext()){
            Fast = data.getString(0);

        }
        return Fast;
    }

    public String getHadith(String date){
        String Hadith = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COL5 + " FROM " + TABLE_NAME +
                " WHERE " + COL3 + " = '" + date + "'";
        Cursor data = db.rawQuery(query, null);
        while(data.moveToNext()){
            Hadith = data.getString(0);

        }
        return Hadith;
    }

    public String getAyat(String date){
        String Ayat = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COL6 + " FROM " + TABLE_NAME +
                " WHERE " + COL3 + " = '" + date + "'";
        Cursor data = db.rawQuery(query, null);
        while(data.moveToNext()){
            Ayat = data.getString(0);

        }
        return Ayat;
    }

    public String getNamaz(String date){
        String Namaz = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COL7 + " FROM " + TABLE_NAME +
                " WHERE " + COL3 + " = '" + date + "'";
        Cursor data = db.rawQuery(query, null);
        while(data.moveToNext()){
            Namaz = data.getString(0);

        }
        return Namaz;
    }

    public void deleteRow(String date){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE "
                + COL3 + " = '" + date + "'";
        db.execSQL(query);
    }

}
