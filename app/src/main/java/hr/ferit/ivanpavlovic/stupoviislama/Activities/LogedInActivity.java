package hr.ferit.ivanpavlovic.stupoviislama.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import hr.ferit.ivanpavlovic.stupoviislama.Login;
import hr.ferit.ivanpavlovic.stupoviislama.MainActivity;
import hr.ferit.ivanpavlovic.stupoviislama.R;
import hr.ferit.ivanpavlovic.stupoviislama.Register;
import hr.ferit.ivanpavlovic.stupoviislama.ayat;
import hr.ferit.ivanpavlovic.stupoviislama.qiblah;
import hr.ferit.ivanpavlovic.stupoviislama.Activities.kompas;

public class LogedInActivity extends AppCompatActivity {

    private static final String TAG = "Loged in activity";
    /*varijabla za provjeru imamo li dobru verziju APIja*/
    private static final int ERROR_DIALOG_REQUEST =9001;
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    public void onBackPressed() {
            onPostResume();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loged_in);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);



        final TextView tvSelam = (TextView) findViewById(R.id.tvSelam);
        Button bAyat = (Button) findViewById(R.id.bAyat_logedin);
        Button bQiblah = (Button) findViewById(R.id.bQIblah);
        Button bNamaz = (Button) findViewById(R.id.bNamaz);
        Button bDnevnik = (Button) findViewById(R.id.bDnevnik);

        Intent intent = getIntent();
        String name =  intent.getStringExtra("name");
        String surname =  intent.getStringExtra("surname");
        String username =  intent.getStringExtra("username");

        /*spremanje korisnickog imena u shared preferences
        * TO DO-izbrisati prijasnju preferencu
        * */
        SharedPreferences.Editor settings = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        settings.putString("name", name);
        settings.apply();

        String message =  "Selam aleykum, "+name;
        tvSelam.setText(message);



        bAyat.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), ayat.class);
                startActivity(explicitIntent);
            }
        });


        /*moze se kliknuti na gumb samo ako je sve u redu sa servisom odnosno dobro postavljenim mapama*/
        if(isServicesOK()) {
            bQiblah.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent explicitIntent = new Intent();
                    explicitIntent.setClass(getApplicationContext(), qiblah.class);
                    startActivity(explicitIntent);
                }
            });
        }


        bNamaz.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), namaz.class);
                startActivity(explicitIntent);
            }
        });

        bDnevnik.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), Diary.class);
                startActivity(explicitIntent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_logout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(LogedInActivity.this);

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            /*google nam daje soluciju za postojani problem u obliku dijaloga*/
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(LogedInActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }else{
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        /*ako postoji problem vraća false*/
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.Register:
                Intent i = new Intent(this, Register.class);
                startActivity(i);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case R.id.Login:
                Intent i2 = new Intent(this, Login.class);
                startActivity(i2);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case R.id.Logout:
                Intent i3 = new Intent(this, MainActivity.class);
                startActivity(i3);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            default:
                //unknown error
        }
        return super.onOptionsItemSelected(item);
    }
}
