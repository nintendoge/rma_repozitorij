package hr.ferit.ivanpavlovic.stupoviislama.klase;

/**
 * Created by Iohannes on 12.8.2018..
 */

public class ExampleItem {
    private int mImageResource;
    private String mText1;
    private String mText2;

    /*konstruktor klase*/
    public ExampleItem(int ImageResource, String text1, String text2){
        mImageResource = ImageResource;
        mText1=text1;
        mText2=text2;
    }

    /*getteri*/
    public int getImageResource(){
        return mImageResource;
    }

    public String getText1(){
        return mText1;
    }

    public String getText2(){
        return mText2;
    }

}
