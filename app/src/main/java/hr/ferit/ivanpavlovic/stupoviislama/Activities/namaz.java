package hr.ferit.ivanpavlovic.stupoviislama.Activities;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Calendar;
import java.util.List;
import android.content.Intent;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
/*AppCompatActivity instead of import android.support.v7.app.ActionBarActivity;*/
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import hr.ferit.ivanpavlovic.stupoviislama.R;
import hr.ferit.ivanpavlovic.stupoviislama.klase.Alarm;
import hr.ferit.ivanpavlovic.stupoviislama.klase.ExampleAdapter;
import hr.ferit.ivanpavlovic.stupoviislama.klase.ExampleItem;
import hr.ferit.ivanpavlovic.stupoviislama.klase.namaz_popup;

public class namaz extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    /*most između podataka i recycler viewa*/
    private ExampleAdapter mAdapter;
    /*aligna elemente viewa*/
    private RecyclerView.LayoutManager mLayoutManager;

    /*builder objekta notifikacija, treba nam id za notifikaciju*/
    NotificationCompat.Builder notification;
    private static final int uniqueID=1684;
    private static final String CHANNEL_ID= "channel_param";
    public boolean[] array = new boolean[6];


    private final void createNotificationChannel(){
        /*version code oreo, 26*/
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
        {
            CharSequence name = "Notifikacije za namaze";
            String description = "Opis namaza";
            /*za androide O nadalje je potrebno definirati importance*/
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,name,importance);

            notificationChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);

        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_namaz);

        Toast.makeText(getApplicationContext(),"Press the prayer name for further information", Toast.LENGTH_LONG).show();

        ArrayList<ExampleItem> exampleList = new ArrayList<>();
        exampleList.add(new ExampleItem(R.drawable.namaz_icon, "Fajar", "Set for 04:20"));
        exampleList.add(new ExampleItem(R.drawable.namaz_icon, "Zohar", "Set for 12:45"));
        exampleList.add(new ExampleItem(R.drawable.namaz_icon, "Asar", "Set for 16:28"));
        exampleList.add(new ExampleItem(R.drawable.namaz_icon, "Moghrib", "Set for 19:24"));
        exampleList.add(new ExampleItem(R.drawable.namaz_icon, "Isha", "Set for 21:03"));
        exampleList.add(new ExampleItem(R.drawable.namaz_icon, "Juma", "Set for 01:13"));

        /*----------------------SETTING_TIME_FOR_PRAYERS----------------------------------*/

        final List<Calendar> time = new ArrayList<Calendar>();

        /* FAJR */
        Calendar date1 = Calendar.getInstance();
        date1.set(Calendar.HOUR_OF_DAY, 4);
        date1.set(Calendar.MINUTE, 20);
        date1.set(Calendar.SECOND, 0);
        date1.set(Calendar.MILLISECOND, 0);
        /*za sutrasnji dan*/
        if (date1.getTimeInMillis() < System.currentTimeMillis())
        {
            date1.add(Calendar. DATE, 1);
        }
        time.add(date1);

        /* DHUHR */
        Calendar date2 = Calendar.getInstance();
        date2.set(Calendar.HOUR_OF_DAY, 12);
        date2.set(Calendar.MINUTE, 45);
        date2.set(Calendar.SECOND, 0);
        date2.set(Calendar.MILLISECOND, 0);
        /*za sutrasnji dan*/
        if (date2.getTimeInMillis() < System.currentTimeMillis())
        {
            date2.add(Calendar. DATE, 1);
        }
        time.add(date2);

        /* ASR */
        Calendar date3 = Calendar.getInstance();
        date3.set(Calendar.HOUR_OF_DAY, 16);
        date3.set(Calendar.MINUTE, 28);
        date3.set(Calendar.SECOND, 0);
        date3.set(Calendar.MILLISECOND, 0);
        /*za sutrasnji dan*/
        if (date3.getTimeInMillis() < System.currentTimeMillis())
        {
            date3.add(Calendar. DATE, 1);
        }
        time.add(date3);

        /* MAGHRIB*/
        Calendar date4 = Calendar.getInstance();
        date4.set(Calendar.HOUR_OF_DAY, 19);
        date4.set(Calendar.MINUTE, 24);
        date4.set(Calendar.SECOND, 0);
        date4.set(Calendar.MILLISECOND, 0);
        /*za sutrasnji dan*/
        if (date4.getTimeInMillis() < System.currentTimeMillis())
        {
            date4.add(Calendar. DATE, 1);
        }
        time.add(date4);

        /* ISHA */
        Calendar date5 = Calendar.getInstance();
        date5.set(Calendar.HOUR_OF_DAY, 20);
        date5.set(Calendar.MINUTE, 25);
        date5.set(Calendar.SECOND, 0);
        date5.set(Calendar.MILLISECOND, 0);
        /*za sutrasnji dan*/
        if (date5.getTimeInMillis() < System.currentTimeMillis())
        {
            date5.add(Calendar. DATE, 1);
        }
        time.add(date5);


        /* QIYAM */
        Calendar date6 = Calendar.getInstance();
        date6.set(Calendar.HOUR_OF_DAY, 21);
        date6.set(Calendar.MINUTE, 3);
        date6.set(Calendar.SECOND, 0);
        date6.set(Calendar.MILLISECOND, 0);
        /*za sutrasnji dan*/
        if (date5.getTimeInMillis() < System.currentTimeMillis())
        {
            date5.add(Calendar. DATE, 1);
        }
        time.add(date5);


        /*----------------------SETTING_TIME_FOR_PRAYERS----------------------------------*/

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ExampleAdapter(exampleList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        createNotificationChannel();


        mAdapter.setOnItemClickListener(new ExampleAdapter.OnItemClickListener() {


            @Override
            public void onToggleClick(int position) {




                /*---------------------------------NOTIFIKACIJE_ZA_KLIK----------------------------------*/
                switch(position) {
                    case 0:
                        setAlarm(time.get(0).getTimeInMillis(),"Fajar",0);
                        array[0]=true;
                        break;
                    case 1:
                        setAlarm(time.get(1).getTimeInMillis(),"Zohar",1);
                        array[1]=true;
                        break;
                    case 2:
                        setAlarm(time.get(2).getTimeInMillis(),"Asar",2);
                        array[2]=true;
                        break;
                    case 3:
                        setAlarm(time.get(3).getTimeInMillis(),"Maghrib",3);
                        array[3]=true;
                        break;
                    case 4:
                        setAlarm(time.get(4).getTimeInMillis(),"Isha",4);
                        array[4]=true;
                        break;
                    case 5:
                        setAlarm(time.get(5).getTimeInMillis(),"Juma",5);
                        array[5]=true;
                        break;
                    default: Toast.makeText(getApplicationContext(), "invalid input for "+position, Toast.LENGTH_LONG).show();
                     break;

                }



            }


            @Override
            public void displayInfo(int position) {
                Intent i = new Intent(namaz.this, namaz_popup.class);
                switch(position) {
                    case 0:
                        i.putExtra("name","Fajar");
                        startActivity(i);
                        break;
                    case 1:
                        i.putExtra("name","Zohar");
                        startActivity(i);
                        break;
                    case 2:
                        i.putExtra("name","Asar");
                        startActivity(i);
                        break;
                    case 3:
                        i.putExtra("name","Moghrib");
                        startActivity(i);
                        break;
                    case 4:
                        i.putExtra("name","Isha");
                        startActivity(i);
                        break;
                    case 5:
                        i.putExtra("name","Juma");
                        startActivity(i);
                        break;
                    default: Toast.makeText(getApplicationContext(), "invalid input for "+position, Toast.LENGTH_LONG).show();
                        break;
                }

            }


            @Override
            public void disableAlarm(int position) {
                /*fix this method*/
                array[position]=false;

                Intent i = new Intent(namaz.this, Alarm.class);
                //PendingIntent.getBroadcast(getApplicationContext(), position, i, PendingIntent.FLAG_ONE_SHOT).cancel();

                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                //PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), position, i, PendingIntent.FLAG_ONE_SHOT);
                PendingIntent.getBroadcast(getApplicationContext(), position, i, 0).cancel();
                alarmManager.cancel(PendingIntent.getBroadcast(getApplicationContext(), position, i, 0));


                /*ispis vremena za koje je isključen alarm/notifikacija*/
                DateFormat formatter = new SimpleDateFormat("HH:mm");
                String dateFormatted = formatter.format(time.get(position).getTimeInMillis());
                Toast.makeText(getApplicationContext(),"Disabled the alarm for "+dateFormatted, Toast.LENGTH_LONG).show();


            }

            /*--------------------------------ALARM-----------------------------------------------------*/
            public void setAlarm(long time,String name,int intentRequest) {
                Intent i = new Intent(namaz.this, Alarm.class);
                i.putExtra("name", name);
                /*dummy metoda da ne dropa extras*/
                i.setAction(Long.toString(System.currentTimeMillis()));
                PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), intentRequest, i, 0);
                AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
                /*RTC_WAKEUP dozvoljava da se app pokrene i kad je mobitel zakljucan*/

                DateFormat formatter = new SimpleDateFormat("HH:mm");
                String dateFormatted = formatter.format(time);

                if (time < System.currentTimeMillis()) {
                    Toast.makeText(getApplicationContext(), "Alarm set for " + dateFormatted+" tomorrow", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Alarm set for " + dateFormatted+" today", Toast.LENGTH_LONG).show();
                }
                am.set(AlarmManager.RTC_WAKEUP,time,pi);


            }
            /*--------------------------------ALARM-----------------------------------------------------*/

        });


    }

    @Override
    protected void onStop() {
        super.onStop();

        int i;
        float counter;
        int temp=0;
        for(i=0;i<6;i++)
        {
            temp+=array[i]?1:0;
        }
        counter = (float) temp/6;

        SharedPreferences.Editor settings = getSharedPreferences("MyPrefsFile", MODE_PRIVATE).edit();
        settings.putFloat("counter", counter);
        settings.apply();
    }
}


